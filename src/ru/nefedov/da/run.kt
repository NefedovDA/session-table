package ru.nefedov.da

private const val GRATING_MSG = "Hi! It's \"Session Table\" from @NefedovDA."

fun runMenu(): Int {
  val modes = listOf(
    1 to "Add new subject",
    2 to "Add progress",
    3 to "Show statistic",
    0 to "Exit"
  )
  println("What do you want to do now?")
  modes.forEach { println("${it.first} - ${it.second}") }
  print("Enter your choose: ")
  while (true) {
    val ans = readLine()?.toInt()
    when {
      modes.filter { it.first == ans }.any() ->
        return ans!! // ans in modes and not null
      else ->
        println("No such mode. \nEnter your choose again: ")
    }
  }
}

fun main() {
  println(GRATING_MSG)
  while (true) {
    when (runMenu()) {
      1 -> addNewSubject()
      2 -> addProgress()
      3 -> showProgress()
      0 -> return println("By!")
    }
  }
}

private const val STATISTIC = "session_table.stat"

fun showProgress() {
  TODO("Get statistic from the file and print it")
}

fun addProgress() {
  TODO("Add progress to statistic array and save it")
}

fun addNewSubject() {
  TODO("Add new subject to statistic array and save it")
}
